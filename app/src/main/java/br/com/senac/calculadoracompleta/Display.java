package br.com.senac.calculadoracompleta;

import android.icu.text.NumberFormat;
import android.widget.TextView;

/**
 * Created by sala304b on 22/08/2017.
 */

public class Display {

    private TextView display ;
    private boolean limpar = true;


    public Display(TextView display){
        this.display = display;
    }

    public TextView getDisplay(){
        return display;
    }

    public boolean isLimpar(){
        return limpar;
    }

    public void setLimpar(boolean limpar){
        this.limpar = limpar;
    }


    public String getText(){
        return this.display.getText().toString();
    }

    public void setText(String texto){
        this.display.setText(texto);
    }


    public void setText(double valor) {
        this.setText(String.valueOf(valor));
    }
}

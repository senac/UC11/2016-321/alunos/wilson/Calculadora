package br.com.senac.calculadoracompleta;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sala304b on 15/08/2017.
 */
public class ClickButton implements View.OnClickListener {

    private Display display;

    public ClickButton() {

    }

    public ClickButton(Display display) {
        this.display = display;
    }


    @Override
    public void onClick(View v) {


        String texto = display.getText();
        Button botao = (Button) v;

        String digito = botao.getText().toString();


        if (display.isLimpar()) {
            display.setText(digito);
            display.setLimpar(false);
        } else {
            if (botao.getId() != R.id.buttonponto) {
                display.setText(texto + digito);
            } else {
                if (!texto.contains(".")) {
                    display.setText(texto + digito);
                }
            }
        }


    }


}


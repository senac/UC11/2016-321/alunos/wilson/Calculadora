package br.com.senac.calculadoracompleta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Calculadora extends AppCompatActivity {



    private TextView btn1;
    private TextView btn2;
    private TextView btn3;
    private TextView btn4;
    private TextView btn5;
    private TextView btn6;
    private TextView btn7;
    private TextView btn8;
    private TextView btn9;
    private TextView btn0;
    private TextView buttponto;
    private TextView buttCE;
    private TextView btnsoma;
    private TextView btnmult;
    private TextView btnsubt;
    private TextView btndivi;

    private TextView txtResultado;

    private Display display ;

    private static final char adicao = '+';
    private static final char subtracao = '-';
    private static final char divisao = '/';
    private static final char multiplicacao = '*';
    private char operacao = (char) Double.NaN;
    private double operando1 ;
    private double operando2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        this.btn1 = (TextView) findViewById(R.id.button1);
        this.btn2 = (TextView) findViewById(R.id.button2);
        this.btn3 = (TextView) findViewById(R.id.button3);
        this.btn4 = (TextView) findViewById(R.id.button4);
        this.btn5 = (TextView) findViewById(R.id.button5);
        this.btn6 = (TextView) findViewById(R.id.button6);
        this.btn7 = (TextView) findViewById(R.id.button7);
        this.btn8 = (TextView) findViewById(R.id.button8);
        this.btn9 = (TextView) findViewById(R.id.button9);
        this.btn0 = (TextView) findViewById(R.id.button0);
        this.btnsoma = (TextView) findViewById(R.id.soma);
        this.btnmult = (TextView) findViewById(R.id.multiplicacao);
        this.btnsubt = (TextView) findViewById(R.id.subtracao);
        this.btndivi = (TextView) findViewById(R.id.divisao);
        this.buttponto = (TextView) findViewById(R.id.buttonponto);
        this.buttCE = (TextView) findViewById(R.id.buttonCE);
        this.txtResultado = (Button) findViewById(R.id.calcular);

        this.txtResultado = (TextView)findViewById(R.id.resultado);

        this.display = new Display(this.txtResultado) ;



        final ClickButton clickButton = new ClickButton(this.display);

        this.btn0.setOnClickListener( clickButton );
        this.btn1.setOnClickListener( clickButton );
        this.btn2.setOnClickListener( clickButton );
        this.btn3.setOnClickListener( clickButton );
        this.btn4.setOnClickListener( clickButton );
        this.btn5.setOnClickListener( clickButton );
        this.btn6.setOnClickListener( clickButton );
        this.btn7.setOnClickListener( clickButton );
        this.btn8.setOnClickListener( clickButton );
        this.btn9.setOnClickListener( clickButton );
        this.btnsoma.setOnClickListener( clickButton );
        this.btnmult.setOnClickListener( clickButton );
        this.btnsubt.setOnClickListener( clickButton );
        this.btndivi.setOnClickListener( clickButton );
        this.buttponto.setOnClickListener( clickButton );



        this.buttCE = (TextView) findViewById(R.id.buttonCE);

        this.buttCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("0");
            }
        });










        this.btnsoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = adicao;
                calculando = false;
                calculo();

            }
        });

        this.btndivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = divisao;
                calculando = false;
                calculo();
            }
        });


        this.btnmult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = multiplicacao;
                calculando = false;
                calculo();
            }
        });


        this.btnsubt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = subtracao;
                calculando = false;
                calculo();
            }
        });

        this.txtResultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setLimpar(false);
                calculando = true;
                calculo();
            }
        });














    }

    private boolean calculando = false;

    private void calculo(){
        if (Double.isNaN(operando1)){
            operando1 = Double.parseDouble(display.getText().toString());

            limpar(false);

        }else{

            if (!display.isLimpar()){


                if (!calculando){
                    operando2 = Double.parseDouble(String.valueOf(display.getText()));
                }


                switch (operacao){
                    case adicao:
                        operando1 = operando1 + operando2;
                        break;
                    case subtracao:
                        operando1 = operando1 - operando2;
                        break;
                    case multiplicacao:
                        operando1 = operando1 * operando2;
                        break;
                    case divisao:
                        operando1 = operando1 / operando2;
                        break;
                }





            }




        }

        limpar(false);

       display.setText(operando1);
    }

    private void limpar(boolean zerar){
        if (zerar){
        display.setText("0");
            operando1 = Double.NaN;
            calculando = false;
        }


        display.setLimpar(true);
    }








}









